const nodemailer = require('nodemailer');
const fs = require('fs');

const recipienst = require('./recipients');

require.extensions['.html'] = function(module, filename) {
  module.exports = fs.readFileSync(filename, 'utf8');
};

const template = require('./html/questionnaire_nl.html');

const sendEmail = () => {
  const transporter = nodemailer.createTransport({
    service: 'gmail',
    auth: {
      user: process.env.EMAIL,
      pass: process.env.PASSWORD
    }
  });

  const options = {
    from: 'fitatu-test@fitatu.com',
    bcc: recipienst,
    subject: 'Waarom ben je gestopt met Fitatu?',
    html: template
  };

  transporter.sendMail(options, (err, data) => {
    if (err) {
      console.log('Error: ', err);
    } else {
      console.log('Success');
    }
  });
};

exports.sendEmail = sendEmail;
