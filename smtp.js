const nodemailer = require('nodemailer');
const fs = require('fs');
const recipienst = require('./recipients');

require.extensions['.html'] = function(module, filename) {
  module.exports = fs.readFileSync(filename, 'utf8');
};

const template = require('./html/questionnaire_nl.html');

const sendEmail = () => {
  const transporter = nodemailer.createTransport({
    host: process.env.SMTP_HOST,
    port: 587,
    secure: false,
    auth: {
      user: process.env.SMTP_USER,
      pass: process.env.SMTP_PASSWORD
    }
  });

  const options = {
    from: 'support@fitatu.com',
    subject: 'Waarom ben je gestopt met Fitatu?',
  };

  recipienst.forEach(recipient => {
    options.html = template.replace(/\[userEmail\]/g, recipient);
    options.bcc = recipient;

    transporter.sendMail(options, (err, data) => {
      if (err) {
        console.log('Error: ', err);
      } else {
        console.log('Success');
      }
    });
  });


};

exports.sendEmail = sendEmail;
